const express = require('express');
const path = require('path');
const router = express.Router();
//const expbars = require('express-handlebars');
const app=express();

const members = [];

// Set up engine with handlebars

//app.engine('handlebars', expbars({defaultLayout:'main'}));
//app.set('view engine','handlebars');

//const {PORT = 5500} = process.env;
const PORT =  5500 || process.env.PORT ;

app.use('/assets', express.static( path.join( __dirname, 'public', 'static')));

app.get('/', (req,res)=>{
    return res.sendFile( path.join( __dirname, 'index.html' ) );
})

const storedcontacts = [];

// Get all contacts
app.get('/storedcontacts', (req,res)=> res.json(storedcontacts));

app.get('/home', (req,res)=>{
    return res.sendFile( path.join( __dirname, 'home.html' ) );
})

app.get('/about', (req,res)=>{
    return res.sendFile( path.join( __dirname, 'about.html' ) );
})

app.get('/contact', (req,res)=>{
    console.log(req)
    return res.sendFile( path.join( __dirname, 'contact.html' ) );
})

app.post('/contact', (req,res)=>{
    console.log(req)
    //return res.send ('gOOD JOB........')
    return res.status(201).json('Contact stored successfully');
})

// Create contact
router.post('/contact', (req,res) => {
    const Newcontact = {
        name: req.body.name,
        email:req.body.email,
        status: 'active'
    }
    storedcontacts.push(Newcontact);
    res.json(storedcontacts);
});

app.get('/menu', (req,res)=>{
    return res.sendFile( path.join( __dirname, 'menu.html' ) );
})

app.get('/*', (req,res)=>{
    return res.send ('Page not found........')
})

app.listen(PORT, ()=> console.log(`Server started on port ${PORT}...`));